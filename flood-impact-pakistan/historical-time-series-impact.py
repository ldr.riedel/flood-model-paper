import argparse
import logging

from flood_model_paper import *

from climada.entity import Exposures, ImpactFunc, ImpactFuncSet
from climada_petals.hazard.rf_glofas import hazard_series_from_dataset
from climada.engine.unsequa import InputVar, CalcImpact

HISTORICAL_TIME_SERIES_PATH = DATA_GEN_DIR / "pakistan-flood-2000-2022-comp.nc"

IMPACT_FUNC_PATH = DATA_GEN_DIR / "impact_function_cross_calibration.json"
IMPACT_FUNC_FLOPROS_PATH = (
    DATA_GEN_DIR / "impact_function_cross_calibration_flopros.json"
)
LOGGER = logging.getLogger(__file__)


def load_hazard(year: int, flopros: bool):
    with xr.open_dataset(
        HISTORICAL_TIME_SERIES_PATH,
        engine="netcdf4",
        chunks=dict(time=1, latitude=-1, longitude=-1, sample=-1),
    ) as ds:
        ds = ds.sel(time=str(year))
        intensity = "flood_depth"
        if flopros:
            intensity = "flood_depth_flopros"
        return hazard_series_from_dataset(ds, intensity=intensity, event_dim="sample")


# Load exposure for a particular year
def load_exposure(year: int):
    worldpop_file = get_pakistan_worlpop_historical(year)
    shapefile_pakistan = gpd.read_file(PAKISTAN_ADMIN2_SHAPEFILE_PATH).to_crs(CRS_WGS)
    exposure = Exposures.from_raster(
        worldpop_file,
        dst_crs=CRS_WGS,
        geometry=[shapefile_pakistan.geometry.unary_union],
    )
    exposure.set_geometry_points()
    # Only retain values > 0
    exposure.gdf = exposure.gdf.loc[exposure.gdf["value"] > 0, :]
    exposure.gdf["impf_RF"] = 1

    return exposure


def calculate_impact_uncertainty(year: int, protection: str, n_workers: int):
    LOGGER.info(f"Calculating impact for {protection}: {year}")

    hazard_series = load_hazard(year, protection == "FLOPROS")

    # Exposure input
    exposure = load_exposure(year)
    exposure.assign_centroids(hazard_series.iloc[0])
    exposure_input = InputVar.exp([exposure])

    # Impf input
    if protection == "FLOPROS":
        impf_param = pd.read_json(IMPACT_FUNC_FLOPROS_PATH)
    else:
        impf_param = pd.read_json(IMPACT_FUNC_PATH)
    impf_input = InputVar.impfset(
        [
            ImpactFuncSet(
                [
                    ImpactFunc.from_step_impf(
                        intensity=(0, param["threshold"], 100),
                        haz_type="RF",
                        paa=(0, param["paa"]),
                    )
                ]
            )
            for _, param in impf_param.iterrows()
        ]
    )

    # Unsequa for each month
    df = []
    for date, hazard in hazard_series.items():
        hazard_input = InputVar.haz([hazard], n_ev=1)
        calc = CalcImpact(exposure_input, impf_input, hazard_input)
        out = calc.make_sample(N=64)
        out = calc.uncertainty(out, processes=n_workers)
        out.aai_agg_unc_df["date"] = date[0]
        df.append(out.aai_agg_unc_df)

    df = pd.concat(df, ignore_index=True)
    df["Protection"] = protection
    return df


def main(args):
    output_dir = DATA_GEN_DIR / "flood-impact-pakistan"
    output_dir.mkdir(exist_ok=True)
    begin, end = args.year_range

    if args.no_protection:
        LOGGER.info("Starting computation for 'No Protection'")
        output_dir = output_dir / "no-protection"
        output_dir.mkdir(exist_ok=True)

        df_list = []
        for year in range(begin, end + 1):
            df = calculate_impact_uncertainty(year, "No Protection", args.n_workers)
            df.to_json(output_dir / f"unsequa-impact-time-series-{year}.json")
            df_list.append(df)
        pd.concat(df_list, ignore_index=True).to_json(
            output_dir / "unsequa-impact-time-series.json"
        )

        # Reset directory
        output_dir = output_dir.parent

    if args.flopros:
        LOGGER.info("Starting computation for 'FLOPROS'")
        output_dir = output_dir / "flopros"
        output_dir.mkdir(exist_ok=True)

        df_list = []
        for year in range(begin, end + 1):
            df = calculate_impact_uncertainty(year, "FLOPROS", args.n_workers)
            df.to_json(output_dir / f"unsequa-impact-time-series-{year}.json")
            df_list.append(df)
        pd.concat(df_list, ignore_index=True).to_json(
            output_dir / "unsequa-impact-time-series.json"
        )


def parse_args():
    parser = argparse.ArgumentParser(
        description="Compute the impact and its uncertainty for the historical "
        "Pakistan flood time series"
    )
    parser.add_argument(
        "year_range",
        type=int,
        nargs=2,
        help="The begin and end years. The last year is included",
    )
    parser.add_argument("n_workers", type=int, help="Number of parallel processes")
    parser.add_argument("--no-protection", action="store_true")
    parser.add_argument("--flopros", action="store_true")
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()
    main(args)
