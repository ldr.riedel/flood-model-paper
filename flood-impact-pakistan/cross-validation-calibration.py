from sklearn.metrics import mean_squared_error, mean_squared_log_error

from climada.util.config import CONFIG
from climada.util.constants import DEF_CRS
from climada.util.calibrate import Input, BayesianOptimizer, OutputEvaluator
from climada.entity import Exposures, ImpactFuncSet, ImpactFunc

from climada_petals.hazard.rf_glofas import (
    hazard_series_from_dataset,
    RiverFloodInundation,
    save_file,
)

import numpy.testing as npt
from multiprocessing import Pool
from copy import deepcopy
from itertools import repeat

from flood_model_paper import *

FLOOD_DSET = DATA_GEN_DIR / "pakistan-flood-220701-220930.nc"


def compute_hazard():
    rf = RiverFloodInundation()
    rf.download_reanalysis(
        "Pakistan",
        2022,
        preprocess=lambda x: x.sel(time=slice("2022-07-01", "2022-09-30")).max(
            dim="time"
        ),
    )
    ds_flood = rf.compute()
    save_file(ds_flood, FLOOD_DSET)


def impf_step_generator(threshold, paa):
    return ImpactFuncSet(
        [
            ImpactFunc.from_step_impf(
                haz_type="RF", intensity=(0, threshold, 100), paa=(0, paa)
            )
        ]
    )


def run_cv_calibration(hazard, exposure, data, sample_size, random_state=1):
    # Draw samples
    adm2 = pd.Series(exposure.gdf["ADM2_EN"].unique())
    adm2_sampled = adm2.sample(sample_size, random_state=random_state)
    adm2_dropped = list(set(adm2).difference(adm2_sampled))

    # Drop those from data that are not in the sample
    data_sampled = data.drop(columns=adm2_dropped)
    data_sampled = data_sampled.set_index(pd.Series([1]))  # Due to hazard.event_id

    inp = Input(
        hazard=hazard,
        exposure=exposure,
        data=data_sampled,
        bounds={"threshold": (0, 3), "paa": (0, 1)},
        cost_func=mean_squared_log_error,
        impact_func_creator=impf_step_generator,
        impact_to_dataframe=lambda impact: impact.impact_at_reg(
            exposure.gdf["ADM2_EN"]
        ).drop(columns=adm2_dropped),
        impact_calc_kwds={"assign_centroids": False, "save_mat": True},
    )

    # Calibrate
    opt = BayesianOptimizer(inp, random_state=random_state, verbose=0)
    output = opt.run(init_points=300, n_iter=100)

    # Check against test data
    impact = OutputEvaluator(inp, output).impact
    error = inp.cost_func(
        data.drop(columns=adm2_sampled),
        impact.impact_at_reg(exposure.gdf["ADM2_EN"]).drop(columns=adm2_sampled),
    )

    # Return information as data frame
    return pd.DataFrame(
        data=output.params | {"Cost Function": -output.target} | {"Error": error},
        index=[0],
    )


def main():
    # Load the true data
    # Create the data frame for the true impact data
    displacement_data = pd.read_csv(SINDH_DISPLACEMENT_DATA_PATH)

    # Mapping from data columns to exposure regions
    data_to_exp = {
        "Kamber": "Kambar Shahdad Kot",
        "Mirpurkhas": "Mirpur Khas",
        "Shaheed Benazir Abad (SBA)": "Shaheed Benazir Abad",
        "Tando Muhammad Khan (TMK)": "Tando Muhammad Khan",
        "Umerkot": "Umer Kot",
    }

    # Rename data based on shapefile and exposure
    displacement_data = displacement_data.rename(columns=data_to_exp)

    # Load the Pakistan shapefile
    shapefile_pakistan = gpd.read_file(PAKISTAN_ADMIN2_SHAPEFILE_PATH).to_crs(CRS_WGS)

    # Carve out Sindh provinces
    sindh_provinces = shapefile_pakistan.loc[shapefile_pakistan["ADM1_EN"] == "Sindh"]

    # Load WorldPop exposure
    WORLD_POP_PAK_PATH = Path(
        "/Users/ldr.riedel/Downloads/pak_ppp_2020_1km_Aggregated.tif"
    )
    exposure = Exposures.from_raster(
        WORLD_POP_PAK_PATH,
        dst_crs=DEF_CRS,
        geometry=[sindh_provinces.geometry.unary_union],
    )
    exposure.set_geometry_points()
    exposure.gdf = exposure.gdf.loc[
        exposure.gdf["value"] > 0, :
    ]  # Only retain values > 0
    exposure.gdf["impf_RF"] = 1

    # Assign Admin2 regions
    gdf_joined = exposure.gdf.sjoin(sindh_provinces, how="left", predicate="within")
    npt.assert_array_equal(
        np.sort(gdf_joined["ADM2_EN"].unique()),
        np.sort(sindh_provinces["ADM2_EN"].unique()),
    )

    # Merge Karachi regions
    adm2 = gdf_joined["ADM2_EN"]
    adm2[adm2.str.contains("Karachi")] = "Karachi"
    gdf_joined["ADM2_EN"] = adm2

    # Set new gdf
    exposure.set_gdf(gdf_joined)

    # List of admin2 regions
    adm2 = pd.Series(exposure.gdf["ADM2_EN"].unique())

    # Load hazard footprints
    with xr.open_dataset(FLOOD_DSET, chunks="auto") as ds:
        ds["time"] = [np.datetime64("2022-09-30")]
        flood_depth = hazard_series_from_dataset(ds, "flood_depth", event_dim="time")
        flood_depth_flopros = hazard_series_from_dataset(
            ds, "flood_depth_flopros", event_dim="time"
        )

    # Perform actual computation
    iterations = 20
    exposures = [exposure.copy() for _ in range(iterations)]
    data = [displacement_data.copy(deep=True) for _ in range(iterations)]

    for idx, hazard in enumerate((flood_depth, flood_depth_flopros)):
        hazards = [deepcopy(hazard) for _ in range(iterations)]
        # random_states = (
        #     np.array(list(range(iterations)), dtype=np.int_) + idx * iterations
        # ).flat
        random_states = range(20)
        with Pool(4) as pool:
            outputs = pool.starmap(
                run_cv_calibration,
                zip(hazards, exposures, data, repeat(20), random_states),
            )

        outputs = pd.concat(outputs, ignore_index=True)
        suffix = "_flopros" if idx > 0 else ""
        outputs.to_json(
            DATA_GEN_DIR / f"impact_function_cross_calibration{suffix}.json"
        )


if __name__ == "__main__":
    compute_hazard()
    main()
