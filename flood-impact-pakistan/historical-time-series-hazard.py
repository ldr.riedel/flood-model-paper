from flood_model_paper import *

from climada_petals.hazard.rf_glofas import RiverFloodInundation, save_file

HISTORICAL_TIME_SERIES_PATH = DATA_GEN_DIR / "pakistan-flood-2000-2022-comp.nc"


def main():
    outdir = DATA_GEN_DIR / "pakistan-flood-2000-2022"
    outdir.mkdir(exist_ok=True)

    # Compute data
    rf = RiverFloodInundation()
    for year in pd.date_range("2000-01-01", "2022-01-01", freq="YS").year:
        rf.download_reanalysis(
            "Pakistan", year, preprocess=lambda x: x.resample(time="1M").max()
        )
        ds_flood = rf.compute(resample_kws=dict(num_bootstrap_samples=20))
        save_file(ds_flood, outdir / f"pakistan-flood-{year}.nc")
        rf.clear_cache()

    # Merge data
    with xr.open_mfdataset(outdir / "*.nc", chunks="auto", concat_dim="time") as ds:
        save_file(ds, HISTORICAL_TIME_SERIES_PATH, zlib=True)

    # Delete intermediates
    outdir.rmdir()


if __name__ == "__main__":
    main()
