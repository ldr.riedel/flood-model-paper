import matplotlib.pyplot as plt
from matplotlib.figure import Figure
from matplotlib.axes import Axes
import matplotlib.patches as mpatches
import matplotlib.colors as mcolors
import matplotlib.ticker as mticker
import matplotlib.lines as mlines
from matplotlib.legend_handler import HandlerTuple

import contextily as ctx
import tol_colors
from cycler import cycler
from matplotlib_scalebar.scalebar import ScaleBar


# MATPLOTLIB
def cm_to_inch(cm):
    return cm * 0.3937007874


fig_width, fig_height = plt.rcParams["figure.figsize"]
figsize_ratio = fig_width / fig_height
FIG_WIDTH_ONE_COL = cm_to_inch(8.3)
FIG_WIDTH_TWO_COL = cm_to_inch(12)
FIG_HEIGHT = FIG_WIDTH_TWO_COL / figsize_ratio

rc = {
    "axes.titlesize": "medium",
    "axes.prop_cycle": cycler(color=tol_colors.tol_cset("bright")),
    "figure.dpi": 600,
    "figure.constrained_layout.use": True,
    "figure.figsize": (FIG_WIDTH_TWO_COL, FIG_HEIGHT),
    "font.family": "Arial",
    "font.size": 9.0,
}
plt.rcParams.update(rc)


PLOT_CRS = "EPSG:3857"  # Spherical mercator

# Set up Stadia maps
toner_lite = ctx.providers.Stadia.StamenTonerLite(
    api_key="d79d0737-493b-438a-9277-14e93a30e8de"
)
toner_labels = ctx.providers.Stadia.StamenTonerLabels(
    api_key="d79d0737-493b-438a-9277-14e93a30e8de"
)
for provider in (toner_lite, toner_labels):
    provider["url"] = provider["url"] + "?api_key={api_key}"

# Define basemaps
BASEMAPS = {
    "stamen": {
        # "providers": (ctx.providers.Stamen.TonerLite, ctx.providers.Stamen.TonerLabels),
        "providers": (toner_lite, toner_labels),
        "attribution": "Map tiles by Stamen Design, under CC BY 3.0.\n© OpenStreetMap, distributed under the Open Data\nCommons Open Database License (ODbL) v1.0.",
    },
    "carto": {
        "providers": (
            ctx.providers.CartoDB.PositronNoLabels,
            ctx.providers.CartoDB.PositronOnlyLabels,
        ),
        "attribution": "© OpenStreetMap contributors © CARTO",
    },
}
BASEMAP_DEFAULT = "stamen"


def add_basemap(ax, provider=BASEMAP_DEFAULT, zoom="auto", crs=PLOT_CRS):
    ctx.add_basemap(
        ax,
        source=BASEMAPS[provider]["providers"][0],
        crs=crs,
        attribution=False,
        zoom=zoom,
        zorder=0,
    )
    ctx.add_basemap(
        ax,
        source=BASEMAPS[provider]["providers"][1],
        crs=crs,
        attribution=False,
        zoom=zoom,
        zorder=49,
    )


def add_attribution(obj, loc, provider=BASEMAP_DEFAULT):
    if isinstance(obj, Figure):
        ax = obj.axes[0]
    elif isinstance(obj, Axes):
        ax = obj
    else:
        raise RuntimeError("Cannot determine axes from object")

    x, ha = 0.01, "left"
    if "right" in loc:
        x, ha = 0.99, "right"

    y, va = 0.01, "bottom"
    if "top" in loc:
        y, va = 0.99, "top"

    ax.text(
        x,
        y,
        BASEMAPS[provider]["attribution"],
        horizontalalignment=ha,
        verticalalignment=va,
        transform=ax.transAxes,
        fontsize=5,
        bbox=dict(boxstyle="Square, pad=0.12", color="white", alpha=0.8),
        zorder=50,
    )


def add_scalebar(ax, loc="lower right"):
    bar = ScaleBar(1, location=loc, box_alpha=0.8)
    bar.set_zorder(50)
    ax.add_artist(bar)


def remove_ticks(ax):
    ax.tick_params(bottom=False, left=False, labelbottom=False, labelleft=False)


def set_margins(obj, margins=0.01):
    """Set margins for all axes in the object"""
    if isinstance(obj, Figure):
        axes = obj.axes
    elif isinstance(obj, Axes):
        axes = [obj]
    else:
        raise RuntimeError("Cannot determine axes from object")

    for ax in axes:
        ax.margins(margins)
