def pick_best_by_ratio(df, by="Cost Function", ratio=0.005):
    thres = df[by].min() + ratio * (df[by].max() - df[by].min())
    return df.loc[df[by] < thres]
