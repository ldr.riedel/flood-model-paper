from pathlib import Path

# PATHS
BASE_DIR = Path(__file__).absolute().parents[2]
DATA_DIR = BASE_DIR / "data"
DATA_GEN_DIR = DATA_DIR / "generated"
DATA_GEN_DIR.mkdir(exist_ok=True)

SATELLITE_WATER_EXTENT_PATH = Path(
    DATA_DIR
    / "VIIRS_20220701_20220831_FloodExtent_PAK"
    / "VIIRS_20220701_20220831_FloodExtent_PAK.shp"
)
PAKISTAN_ADMIN2_SHAPEFILE_PATH = Path(
    DATA_DIR / "pak_adm_wfp_20220909_shp" / "pak_admbnda_adm2_wfp_20220909.shp"
)
PAKISTAN_WORLDPOP_PATH = Path(DATA_DIR / "pak_ppp_2020_1km_Aggregated.tif")
SINDH_DISPLACEMENT_DATA_PATH = Path(DATA_DIR / "sindh-discplacement.csv")

PAKISTAN_WORLDPOP_HISTORICAL = {
    2000
    + idx: DATA_DIR
    / "worldpop_pak_ppp_1km"
    / f"pak_ppp_20{idx:02d}_1km_Aggregated_UNadj.tif"
    for idx in range(20 + 1)
}


def get_pakistan_worlpop_historical(year: int):
    try:
        return PAKISTAN_WORLDPOP_HISTORICAL[year]
    except KeyError as err:
        if year > 2020:
            return PAKISTAN_WORLDPOP_HISTORICAL[2020]
        else:
            raise err
