from climada.util.constants import DEF_CRS

CRS_WGS = DEF_CRS  # WGS
CRS_EA = "ESRI:54034"  # Cylindrical Equal Area

arcsec = 1 / 60 / 60
