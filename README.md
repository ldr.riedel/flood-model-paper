# Module for Flood Model Paper

## Installation

Install CLIMADA following the *advanced* [installation instructions](https://climada-python.readthedocs.io/en/latest/guide/install.html).

Then, activate the CLIMADA Conda environment and install this package:

```shell
mamba activate climada_env
cd flood-model-paper
python -m pip install -e ./
```

Before you continue, please follow the Preparations instructions in the [docs of the GloFAS River Flood module](https://climada-petals--64.org.readthedocs.build/en/64/glofas_rf.html).
